﻿using System;
using System.Text;
using TraceLogic;

namespace Rokolabs.MeasuringTestApp_CUITests
{
	public class TraceSummary
	{
		public static void PrintReport(TraceContext traceContext)
		{
			var testTypes = Enum.GetValues(typeof(TestTypes));
			var stringBuilder = new StringBuilder("Title\tTotal  Max  Min  Avg  Count\n");
			foreach (var testType in testTypes)
			{
				var testReport = traceContext.GetTraceReport((TestTypes) testType);
				stringBuilder.AppendLine(
					$"{testReport.TestType} {testReport.Total:F0} {testReport.Max:F0} {testReport.Min:F0} {testReport.Avg:F0} {testReport.Count}");
			}
			Console.WriteLine(stringBuilder);
		}
	}
}