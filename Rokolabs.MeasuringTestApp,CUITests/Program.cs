﻿using System;

namespace Rokolabs.MeasuringTestApp_CUITests
{
	class Program
	{
		static void Main(string[] args)
		{
			var tests = new MeasureTests();
			Console.WriteLine("Task min");
			tests.MeasureMultipleCalls();
			Console.WriteLine("Task max");
			tests.MeasureMultipleCallsWithParams();
			Console.ReadKey();
		}
	}
}