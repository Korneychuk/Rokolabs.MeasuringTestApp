﻿using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using TraceLogic;

namespace Rokolabs.MeasuringTestApp_CUITests
{
	public class MeasureTests
	{
		private static readonly Random Random = new Random();

		public void Test1000(TraceContext traceContext, int additionalDelay)
		{
			TraceItem traceItem = traceContext.StartTraceItem();
			Thread.Sleep(1000 + additionalDelay);
			traceItem.Complete(TestTypes.Test1000, additionalDelay.ToString());
		}

		public void Test2000(TraceContext traceContext, int additionalDelay)
		{
			TraceItem traceItem = traceContext.StartTraceItem();
			Thread.Sleep(2000 + additionalDelay);
			traceItem.Complete(TestTypes.Test2000, additionalDelay.ToString());
		}

		public void Test3000(TraceContext traceContext, int additionalDelay)
		{
			TraceItem traceItem = traceContext.StartTraceItem();
			Thread.Sleep(3000 + additionalDelay);
			traceItem.Complete(TestTypes.Test3000, additionalDelay.ToString());
		}

		public void MeasureMultipleCalls()
		{
			var traceContext = new TraceContext();
			Test1000(traceContext, 100);
			Test1000(traceContext, 200);
			Test1000(traceContext, 300);
			Console.WriteLine(traceContext.ToString());
		}

		public void MeasureMultipleCallsWithParams()
		{
			int lifetime = Convert.ToInt32(ConfigurationManager.AppSettings["Lifetime"]);
			int threadsCount = Convert.ToInt32(ConfigurationManager.AppSettings["ThreadsCount"]);

			var traceContext = new TraceContext();
			var cancelTokenSource = new CancellationTokenSource();
			var token = cancelTokenSource.Token;

			new Task(() =>
			{
				Thread.Sleep(TimeSpan.FromSeconds(lifetime));
				cancelTokenSource.Cancel();
				TraceSummary.PrintReport(traceContext);
			}).Start();

			for (int i = 0; i < threadsCount; i++)
			{
				Task.Factory.StartNew(() => RunRandomTest(traceContext, token),
					TaskCreationOptions.LongRunning);
			}
		}

		public void RunRandomTest(TraceContext traceContext, CancellationToken token)
		{
			while (!token.IsCancellationRequested)
			{
				switch (Random.Next(1, 4))
				{
					case 1:
						Test1000(traceContext, Random.Next(0, 1000));
						break;
					case 2:
						Test2000(traceContext, Random.Next(0, 1000));
						break;
					case 3:
						Test3000(traceContext, Random.Next(0, 1000));
						break;
				}
			}
		}
	}
}