﻿using System.Collections.Generic;
using System.Linq;

namespace TraceLogic
{
	public class Report
	{

		public TestTypes TestType { get; set; }

		public double Total { get; set; }

		public double Max { get; set; }

		public double Min { get; set; }

		public double Avg { get; set; }

		public int Count { get; set; }

		public Report(IEnumerable<TraceItem> traceItems, TestTypes testType)
		{
			TestType = testType;
			Count = traceItems.Count();
			Min = traceItems.Min(x => x.Duration);
			Max = traceItems.Max(x => x.Duration);
			Avg = traceItems.Average(x => x.Duration);
			traceItems.ToList().ForEach(x => Total += x.Duration);
		}
	}
}