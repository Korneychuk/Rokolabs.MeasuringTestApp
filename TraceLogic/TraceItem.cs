﻿using System.Diagnostics;

namespace TraceLogic
{
	public class TraceItem
	{
		internal double StartedAt { get; }

		internal double Duration { get; private set; }

		internal string Content { get; private set; }

		internal TestTypes TestType { get; private set; }

		private readonly Stopwatch _stopWatch;

		public TraceItem(double startedAt)
		{
			StartedAt = startedAt;
			_stopWatch = new Stopwatch();
			_stopWatch.Start();
		}

		public void Complete(TestTypes testType, string text)
		{
			_stopWatch.Stop();
			Content = $"{testType} {text}";
			Duration = _stopWatch.Elapsed.TotalMilliseconds;
			TestType = testType;
		}
	}
}