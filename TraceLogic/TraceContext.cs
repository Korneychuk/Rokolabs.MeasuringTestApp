﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TraceLogic
{
	public class TraceContext
	{
		private readonly List<TraceItem> _traceItems;
		private readonly Stopwatch _stopWatch;

		private static readonly object Locker = new object();

		public TraceContext()
		{
			_traceItems = new List<TraceItem>();
			_stopWatch = new Stopwatch();
		}

		public TraceItem StartTraceItem()
		{
			if (!_stopWatch.IsRunning)
			{
				_stopWatch.Start();
			}
			lock (Locker)
			{
				var traceItem = new TraceItem(_stopWatch.Elapsed.TotalMilliseconds);
				_traceItems.Add(traceItem);
				return traceItem;
			}
		}

		public Report GetTraceReport(TestTypes testType)
		{
			var traceItems = _traceItems.Where(x => x.Duration > 0 && x.TestType == testType);
			return new Report(traceItems, testType);
		}

		public override string ToString()
		{
			var report = new StringBuilder();
			report.AppendLine($"Total time: {_stopWatch.Elapsed.TotalMilliseconds} ms \n");

			for (int i = 0; i < _traceItems.Count; i++)
			{
				report.AppendLine($"{i + 1}. Started at {_traceItems[i].StartedAt}, duration {_traceItems[i].Duration} \n");
				report.AppendLine($"{_traceItems[i].Content} \n");
			}

			return report.ToString();
		}
	}
}